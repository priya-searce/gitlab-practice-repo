/* Google Compute VM *//

resource "google_compute_instance" "gcp_instance" {
  name = "priya-rhel-tf-vm"
  machine_type = "e2-micro"
  labels = {
    "owner" = "priya-soni"
  }
  boot_disk {
    initialize_params {
      image = "rhel-cloud/rhel-8" 
      size  = 20  
    }
  }
  network_interface {
    network = var.network
    subnetwork = var.vpc_subnetwork
  }
  zone = "asia-east2-a"
}
