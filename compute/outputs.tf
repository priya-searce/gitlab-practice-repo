//* OUTPUTS *//

output "name" {
  description = "Name of Instance"
  value       = google_compute_instance.gcp_instance.name
}

output "compute_instance_id" {
  description = "Compute instance id"
  value       = google_compute_instance.gcp_instance.id
}

output "machine_zone" {
  description = "Compute instance zone"
  value       = google_compute_instance.gcp_instance.zone
}
