//* Global variables *//

variable "project_id" {
  type        = string
  description = "The GCP project ID"
}

variable "machine_type" {
  description = "Machine type to create, e.g. n1-standard-1"
}

variable "vpc_subnetwork" {
  description = "Name of the VPC network to peer."
  type        = string
}

variable "network" {
  description = "network"
}


variable "region" {
  type        = string
  description = "Region where the instance template and instance should be created."
}


